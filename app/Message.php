<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'parent_id',
        'child_id',
        'message',
    ];

    public $timestamps = false;

    public function parent()
    {
        return $this->hasOne('App\User', 'id', 'parent_id');

    }
    public function child()
    {
        return $this->hasOne('App\User', 'id', 'child_id');

    }
}
