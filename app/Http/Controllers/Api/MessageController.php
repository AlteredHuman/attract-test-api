<?php
/**
 * Created by PhpStorm.
 * User: HeadHunter
 * Date: 28.11.2019
 * Time: 13:25
 */

namespace App\Http\Controllers\Api;




use App\Message;
use App\User;
use Illuminate\Http\Request;

class MessageController
{

    public function send(Request $request){

        $requestData = $request->validate([
            'message' => 'max:255|required',
            'user_id' => 'integer|required'
        ]);

        $message = new Message;
        $message->parent_id = $request->user()->id;
        $message->child_id = $request->user_id;
        $message->message = $request->message;
        $message->save();

        return response(['message' => 'Message send success!']);


    }

    public function messageFromUser(Request $request){
        $messages = Message::query()
            ->where('child_id', '=', $request->user()->id)
            ->where('parent_id', '=', $request->user_id)
            ->get();
        $user = User::query()
            ->where('id', '=', $request->user_id)
            ->get();
        return response(['Message from user id:'.$request->user_id => $messages]);
    }
}