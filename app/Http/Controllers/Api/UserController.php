<?php
/**
 * Created by PhpStorm.
 * User: HeadHunter
 * Date: 28.11.2019
 * Time: 13:25
 */

namespace App\Http\Controllers\Api;


use App\User;
use Illuminate\Http\Request;

class UserController
{
    public function getUsers(Request $request){

        $user = User::query()->where('id','!=', $request->user()->id)->get();

        return response(['users'=> $user]);
    }
}