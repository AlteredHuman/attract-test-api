#How to install app:

1.Git clone this repo to your local storege

2.Run composer install

3.Rename file .env.example to .env<br>

4.Set you database settings in file .env

5. run - php artisan migrate

6. try to use api.<br>
<hr>
#Test api URL's
## Register new user:

#####localhost/api/register

####Headers

Accept : application/json

####Post fields:

name

email

password

password_confirmation

## Login user, get oauth token:

#####localhost/api/login
####Headers

Accept : application/json

####Post fields

email

password


## Get all users except you:
#####localhost/api/users <br>
####Headers
Accept : application/json <br>
Authorization : Bearer your-oauth-token<br><br>
####Post
null
## Send message to any user by id:
#####localhost/api/message/send<br>
####Headers
Accept : application/json

Authorization : Bearer your-oauth-token<br><br>
####Post
user_id

message
## Show all message to you from any user by id:
#####localhost/api/message/show<br>
####Headers
Accept : application/json

Authorization : Bearer your-oauth-token<br><br>
####Post
user_id


